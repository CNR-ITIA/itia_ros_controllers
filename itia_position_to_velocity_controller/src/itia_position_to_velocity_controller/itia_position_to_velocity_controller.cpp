#include <itia_position_to_velocity_controller/itia_position_to_velocity_controller.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(itia::control::PositionToVelocityController, controller_interface::ControllerBase);
PLUGINLIB_EXPORT_CLASS(itia::control::PositionToVelocityControllerFfw, controller_interface::ControllerBase);

namespace itia
{
namespace control
{
		

bool PositionToVelocityController::init(hardware_interface::VelocityJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_hw=hw;
  if (!ctrl.init(root_nh,controller_nh))
  {
    ROS_FATAL("ERROR");
    return false;
  }

  bool flag=false;
  m_joint_name=ctrl.getJointName();
  for (unsigned idx=0;idx<m_hw->getNames().size();idx++)
  {
    if (!m_hw->getNames().at(idx).compare(m_joint_name))
    {
      m_jh=m_hw->getHandle(m_joint_name);
      flag=true;
      break;
    }
  }
  if (!flag)
  {
    ROS_FATAL("ERROR");
    return false;
  }
  return true;
}

void PositionToVelocityController::starting(const ros::Time& time)
{
  // CHECK IF JOINT NAME IS PRESENT
  double fb_pos=m_jh.getPosition();
  double fb_vel=m_jh.getVelocity();
  ctrl.starting(time,fb_pos,fb_vel);
  
}

void PositionToVelocityController::stopping(const ros::Time& time)
{
  ctrl.stopping(time);
  m_jh.setCommand(0);
}

void PositionToVelocityController::update(const ros::Time& time, const ros::Duration& period)
{
  double fb_pos=m_jh.getPosition();
  double fb_vel=m_jh.getVelocity();
  
  try
  {
    ctrl.update(time,period,fb_pos,fb_vel);
    m_jh.setCommand(ctrl.getVelCmd());
  }
  catch (...)
  {
    ROS_WARN("something wrong: Controller '%s'",m_controller_nh.getNamespace().c_str());
    m_jh.setCommand(0);
  }
  
}

bool PositionToVelocityControllerFfw::init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_hw=hw;
  if (!ctrl.init(root_nh,controller_nh))
  {
    ROS_ERROR("unable to initialize controller");
    return false;
  }
  bool flag=false;
  m_joint_name=ctrl.getJointName();
  for (unsigned idx=0;idx<m_hw->getNames().size();idx++)
  {
    if (!m_hw->getNames().at(idx).compare(m_joint_name))
    {
      m_jh=m_hw->getHandle(m_joint_name);
      flag=true;
      break;
    }
  }
  if (!flag)
  {
    ROS_FATAL("ERROR");
    return false;
  }
  return true;
}

void PositionToVelocityControllerFfw::starting(const ros::Time& time)
{
  // CHECK IF JOINT NAME IS PRESENT
  double fb_pos=m_jh.getPosition();
  double fb_vel=m_jh.getVelocity();
  ctrl.starting(time,fb_pos,fb_vel);
  
}

void PositionToVelocityControllerFfw::stopping(const ros::Time& time)
{
  ctrl.stopping(time);
  m_jh.setCommandVelocity(0);
  m_jh.setCommandEffort(0);
}

void PositionToVelocityControllerFfw::update(const ros::Time& time, const ros::Duration& period)
{
  double fb_pos=m_jh.getPosition();
  double fb_vel=m_jh.getVelocity();
  
  try
  {
    ctrl.update(time,period,fb_pos,fb_vel);
    m_jh.setCommandPosition(ctrl.getPosCmd());
    m_jh.setCommandVelocity(ctrl.getVelCmd());
    m_jh.setCommandEffort(ctrl.getEffCmd());
  }
  catch (...)
  {
    ROS_WARN("something wrong: Controller '%s'",m_controller_nh.getNamespace().c_str());
    m_jh.setCommandVelocity(0);
    m_jh.setCommandEffort(0);
  }
  
}
		
}
}
