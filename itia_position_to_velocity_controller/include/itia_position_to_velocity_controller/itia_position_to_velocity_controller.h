#ifndef __itia_pos_to_vel_control__
#define __itia_pos_to_vel_control__

#include <itia_position_to_velocity_controller/itia_position_to_velocity_math.h>
#include <hardware_interface/joint_command_interface.h>
#include <itia_basic_hardware_interface/posveleff_command_interface.h>

#include <hardware_interface/posvelacc_command_interface.h>

namespace itia
{
namespace control
{

class PositionToVelocityController : public controller_interface::Controller<hardware_interface::VelocityJointInterface>
{
public:
  bool init(hardware_interface::VelocityJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  void update(const ros::Time& time, const ros::Duration& period);
  void starting(const ros::Time& time);
  void stopping(const ros::Time& time);
  
  
protected:
  
  itia::control::PositionToVelocityControllerMath ctrl;
  hardware_interface::VelocityJointInterface* m_hw;
  hardware_interface::JointHandle m_jh;
  
  double m_pos_cmd;
  double m_vel_cmd;
  double m_eff_cmd;
  
  ros::NodeHandle m_root_nh;
  ros::NodeHandle m_controller_nh;
  std::string m_joint_name;
};

class PositionToVelocityControllerFfw : public controller_interface::Controller<hardware_interface::PosVelEffJointInterface>
{
public:
  bool init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  void update(const ros::Time& time, const ros::Duration& period);
  void starting(const ros::Time& time);
  void stopping(const ros::Time& time);
  
  
protected:
  
  itia::control::PositionToVelocityControllerMath ctrl;
  hardware_interface::PosVelEffJointInterface* m_hw;
  hardware_interface::PosVelEffJointHandle m_jh;
  
  double m_pos_cmd;
  double m_vel_cmd;
  double m_eff_cmd;
  
  ros::NodeHandle m_root_nh;
  ros::NodeHandle m_controller_nh;
  std::string m_joint_name;
};

}
}

# endif