#ifndef AnalogStatePublisher_201901210739
#define AnalogStatePublisher_201901210739

#include <controller_interface/controller.h>
#include <itia_basic_hardware_interface/analog_state_interface.h>
#include <std_msgs/Float64MultiArray.h>
#include <ros/ros.h>

namespace ros
{
namespace control
{
  
  class AnalogStatePublisher: public controller_interface::Controller<hardware_interface::AnalogStateInterface>
  {
  public:
    virtual bool init(hardware_interface::AnalogStateInterface* hw,
                      ros::NodeHandle&                          root_nh,
                      ros::NodeHandle&                          controller_nh);
    virtual void starting(const ros::Time& time);
    virtual void update(const ros::Time& time, const ros::Duration& period);
    virtual void stopping(const ros::Time& time);
    
  protected:
    hardware_interface::AnalogStateInterface* m_hw;
    std::vector<std::string> m_analog_names;
    std::string m_topic_name;
    
    ros::Publisher m_pub;
    std::size_t m_nax;
  };
}
}


#endif
