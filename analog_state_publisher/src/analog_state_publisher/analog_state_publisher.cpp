#include <analog_state_publisher/analog_state_publisher.h>
#include <pluginlib/class_list_macros.h> // header for PLUGINLIB_EXPORT_CLASS. NOTE IT SHOULD STAY IN THE CPP FILE NOTE 
PLUGINLIB_EXPORT_CLASS(ros::control::AnalogStatePublisher, controller_interface::ControllerBase);

namespace ros
{
  namespace control
  {
    
    bool AnalogStatePublisher::init ( hardware_interface::AnalogStateInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh )
    {
      /* called immediately after the constructor */
      m_hw=hw;
      if (!controller_nh.getParam("controlled_joint",m_analog_names))
      {
        ROS_FATAL("ERROR");
        return false;
      }
      if (!controller_nh.getParam("topic",m_topic_name))
      {
        ROS_FATAL("ERROR");
        return false;
      }

      
      m_nax=m_analog_names.size();

      for (std::string name: m_analog_names)
      {
        try 
        {
          m_hw->getHandle(name);
        }
        catch(...)
        {
          ROS_ERROR("Resource named %s is not managed by hardware_interface",name.c_str());
          ROS_ERROR("Available resources: ");
          for(auto const & n : m_hw->getNames() )
          {
              ROS_ERROR("- %s", n.c_str() );
          }
          return false;
        }
      }
      
      m_pub =  controller_nh.advertise<std_msgs::Float64MultiArray>(m_topic_name,1);
      return true;
    }
    
    void AnalogStatePublisher::starting ( const ros::Time& time )
    {
      
    }
    
    void AnalogStatePublisher::update ( const ros::Time& time, const ros::Duration& period )
    {
      std_msgs::Float64MultiArrayPtr msg(new std_msgs::Float64MultiArray());
      msg->data.resize(m_nax,0);
      msg->layout.dim.resize( m_nax );
      
      for (std::size_t idx=0;idx<m_nax;idx++)
      {
        msg->layout.dim.at(idx).label = m_analog_names.at(idx);
        msg->data.at(idx)=m_hw->getHandle(m_analog_names.at(idx)).getValue();
      }
      m_pub.publish(msg);
    }
    
    void AnalogStatePublisher::stopping ( const ros::Time& time )
    {
      
      
    }
    
    
    
  }
}
