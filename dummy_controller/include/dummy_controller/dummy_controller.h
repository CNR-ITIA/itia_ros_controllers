#ifndef __dummy_controller__
#define __dummy_controller__

#include <controller_interface/controller.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
namespace itia
{
namespace control
{
  
class DummyController: public controller_interface::Controller<hardware_interface::JointStateInterface>
{
public:
  virtual bool init(hardware_interface::JointStateInterface* hw,
                    ros::NodeHandle&                         root_nh,
                    ros::NodeHandle&                         controller_nh);
  virtual void starting(const ros::Time& time);
  virtual void update(const ros::Time& time, const ros::Duration& period);
  virtual void stopping(const ros::Time& time);
  
private:
  hardware_interface::JointStateInterface* m_hw;
  hardware_interface::JointStateHandle m_jh;
  std::string m_joint_name;
  
  
};

  
}
}

# endif