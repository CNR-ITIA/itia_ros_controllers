#include <dummy_controller/dummy_controller.h>

#include <pluginlib/class_list_macros.h> // header for PLUGINLIB_EXPORT_CLASS. NOTE IT SHOULD STAY IN THE CPP FILE NOTE 
PLUGINLIB_EXPORT_CLASS(itia::control::DummyController, controller_interface::ControllerBase);

namespace itia
{
namespace control
{

bool DummyController::init ( hardware_interface::JointStateInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh )
{
  /* called immediately after the constructor */
  m_hw=hw;
  if (!controller_nh.getParam("controlled_joint",m_joint_name))
  {
    ROS_FATAL("ERROR");
    return false;
  }
  bool flag=false;
  for (unsigned idx=0;idx<m_hw->getNames().size();idx++)
  {
    if (!m_hw->getNames().at(idx).compare(m_joint_name))
    {
      m_jh=m_hw->getHandle(m_joint_name);
      flag=true;
      break;
    }
  }
  if (!flag)
  {
    ROS_FATAL("ERROR");
    return false;
  }
  
  
  return true;
}

void DummyController::starting ( const ros::Time& time )
{
  /* called immediately before starting the control loop */
}

void DummyController::update ( const ros::Time& time, const ros::Duration& period )
{
  /* called every iteration when the controller is active*/
}

void DummyController::stopping ( const ros::Time& time )
{
  /* called before the destructor*/
  
}


  
}
}