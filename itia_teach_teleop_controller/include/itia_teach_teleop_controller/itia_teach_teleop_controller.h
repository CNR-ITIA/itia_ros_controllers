#ifndef itia_teach_teleop_controller__20191151155
#define itia_teach_teleop_controller__20191151155

# include <ros/ros.h>
# include <ros/package.h>
# include <controller_interface/controller.h>
# include <hardware_interface/joint_command_interface.h>
# include <itia_basic_hardware_interface/posveleff_command_interface.h>
# include <sensor_msgs/JointState.h>

# include <subscription_notifier/subscription_notifier.h>
# include <eigen_state_space_systems/eigen_state_space_systems.h>
# include <eigen_state_space_systems/eigen_controllers.h>

#include <urdf_model/model.h>
#include <urdf_parser/urdf_parser.h>
#include <diagnostic_msgs/KeyValue.h>



namespace itia
{
namespace control
{
class TeachTeleopController: public controller_interface::Controller<hardware_interface::PosVelEffJointInterface>
{

public:
    TeachTeleopController();
    bool init ( hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh );
    void update ( const ros::Time& time, const ros::Duration& period );
    void starting ( const ros::Time& time );
    void stopping ( const ros::Time& time );
    bool callback(const diagnostic_msgs::KeyValueConstPtr msg);
    
protected:
    hardware_interface::PosVelEffJointInterface* m_hw;
    std::vector<hardware_interface::PosVelEffJointHandle> m_jh;
    
    ros::NodeHandle m_nh;
    ros::NodeHandle m_root_nh;
    ros::NodeHandle m_controller_nh;
    std::vector<std::string> m_joint_names;
    unsigned int m_nAx;

    std::vector<double> m_target_vel;
    std::vector<double> m_target_pos;
    std::vector<double> m_cmd_pos_old;
    std::vector<double> m_cmd_pos;
    
    std::vector<double> m_upper_limit;
    std::vector<double> m_lower_limit;
        
    double m_time;
    bool m_configured;
    bool m_compared;
    bool m_move = false;
    
    urdf::ModelInterfaceSharedPtr m_model;
    
    double m_err;
    double m_err_old = 0;
    std::string m_path_to_config;
    

    std::shared_ptr<ros_helper::SubscriptionNotifier<diagnostic_msgs::KeyValue>> m_joint_target_rec;






};
}
}







#endif
