#include <itia_teach_teleop_controller/itia_teach_teleop_controller.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(itia::control::TeachTeleopController, controller_interface::ControllerBase);


namespace itia
{
namespace control
{
TeachTeleopController::TeachTeleopController()
{
}


bool TeachTeleopController::init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  ROS_DEBUG("starting TeachTeleopController '%s'",m_controller_nh.getNamespace().c_str());

  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_hw = hw;


  if (!m_controller_nh.getParam("controlled_joints", m_joint_names))
  {
    ROS_FATAL_STREAM(m_controller_nh.getNamespace()+"/'controlled_joints' does not exist");
    ROS_FATAL("ERROR DURING INITIALIZATION CONTROLLER '%s'", m_controller_nh.getNamespace().c_str());
    return false;
  }

  m_nAx=m_joint_names.size();


  std::string robot_description;
  if (!m_nh.getParam("/robot_description", robot_description))
  {
    ROS_FATAL_STREAM("Parameter '/robot_description' does not exist");
    return false;
  }
  m_model = urdf::parseURDF(robot_description);
  
  m_path_to_config = ros::package::getPath("robot_teleop_gui")+"/config";

  //Check if all controlled_joints are defined in /robot_description URDF
  for (unsigned int iAx=0; iAx<m_nAx; iAx++)
  {
    m_configured = false;
    for (const std::pair<std::string, urdf::JointSharedPtr>& joint_tuple: m_model->joints_)
    {
      if (!m_joint_names.at(iAx).compare(joint_tuple.second->name))
        m_configured = true;
    }
    
    if (!m_configured)
    {
      ROS_ERROR("The joint named %s in %s/controlled_joints is not defined in /robot_description URDF", m_joint_names.at(iAx).c_str(), m_controller_nh.getNamespace().c_str());
      return false;
    }
  }
  

  m_upper_limit.resize(m_nAx);
  m_lower_limit.resize(m_nAx);
  for (unsigned int iAx=0; iAx<m_nAx; iAx++)
  {
    m_upper_limit.at(iAx) = m_model->getJoint(m_joint_names.at(iAx))->limits->upper;
    m_lower_limit.at(iAx) = m_model->getJoint(m_joint_names.at(iAx))->limits->lower;
  }
  






  //INIT PUB/SUB
  m_jh.resize(m_nAx);
  m_target_vel.resize(m_nAx);
  m_target_pos.resize(m_nAx);
  m_cmd_pos_old.resize(m_nAx);
  m_cmd_pos.resize(m_nAx);
  
  for (unsigned int idx=0; idx<m_nAx; idx++)
    m_jh.at(idx)=m_hw->getHandle(m_joint_names.at(idx));

  for (unsigned int idx=0; idx<m_nAx; idx++)
  {
    m_target_vel.at(idx)=0;
    m_target_pos.at(idx)=m_jh.at(idx).getPosition();
    m_cmd_pos.at(idx)=m_jh.at(idx).getPosition();
  }

  std::string setpoint_topic_name = "/robot_teleop_gui/teach_key_value";
  m_joint_target_rec.reset(new ros_helper::SubscriptionNotifier<diagnostic_msgs::KeyValue>(m_controller_nh,setpoint_topic_name, 1,boost::bind(&TeachTeleopController::callback,this,_1)));

  ROS_INFO("TeachTeleopController '%s' well initialized",m_controller_nh.getNamespace().c_str());
  return true;

}


void TeachTeleopController::starting(const ros::Time& time)
{
  for (unsigned int idx=0.0; idx<m_nAx; idx++)
  {
    m_target_vel.at(idx)=0.25;//0.0;
    m_target_pos.at(idx)=m_jh.at(idx).getPosition();
  }
  ROS_INFO("[ %s ] Starting controller",  m_controller_nh.getNamespace().c_str());
}


void TeachTeleopController::stopping(const ros::Time& time)
{
  for (unsigned int idx=0; idx<m_nAx; idx++)
    m_target_vel.at(idx)=0.0;
  
  ROS_INFO("[ %s ] Stopping controller",  m_controller_nh.getNamespace().c_str());
}



void TeachTeleopController::update(const ros::Time& time, const ros::Duration& period)
{
  m_time=period.toSec();
  for (unsigned int idx=0; idx<m_nAx; idx++)
  {
    if (m_jh.at(idx).getPosition() > m_upper_limit.at(idx) && m_upper_limit.at(idx)!= 0)
    {
      ROS_WARN("MAXIMUM joint limit reached");
      m_target_vel.at(idx)=0.0;
    }

    if (m_jh.at(idx).getPosition() < m_lower_limit.at(idx) && m_lower_limit.at(idx)!= 0)
    {
      ROS_WARN("MINIMUM joint limit reached");
      m_target_vel.at(idx)=0.0;
    }


    std::vector<double> target_vel=m_target_vel;
    if (!m_target_pos.empty())  //Check necessary to stop at set-point reached
    {
      m_err = m_target_pos.at(idx)-m_cmd_pos.at(idx);
      
      if (abs(m_err)>0.0)
        target_vel.at(idx)=target_vel.at(idx)*(abs(m_err)/m_err);
      else
        target_vel.at(idx)=0.0;
    }
    
    if (!m_move)
      target_vel.at(idx)=0.0;
      
    m_cmd_pos.at(idx) = m_cmd_pos.at(idx) + target_vel.at(idx)*m_time; //p(k)=p(k+1)+v*t;
    if (m_joint_target_rec->isANewDataAvailable())
      m_jh.at(idx).setCommandPosition(m_cmd_pos.at(idx));
  }
}




bool TeachTeleopController::callback(const diagnostic_msgs::KeyValueConstPtr msg)
{ 
  
  //Save teach position info parameter on a .YAML file
  if (!strcmp(msg->key.c_str(), "Save"))
  {
    //Store teach position info into a parameter
    std::string teach_parameter_name = "/teach_teleop_info_position/controlled_joints";
    std::string string_to_save_yaml =  "rosparam set "+teach_parameter_name+" \"[" ;
    for (int i=0; i<m_joint_names.size()-1; i++)
      string_to_save_yaml.append(m_joint_names.at(i)+",");

    string_to_save_yaml.append(m_joint_names.back()+"]\" ");

    if (system( string_to_save_yaml.c_str() )!=0)
      ROS_WARN("Unable to store yaml parameters");
    
    teach_parameter_name = "/teach_teleop_info_position/target_pos";
    string_to_save_yaml =  "rosparam set "+teach_parameter_name+" \"[" ;
    for (int i=0; i<m_joint_names.size()-1; i++)
      string_to_save_yaml.append(std::to_string(m_jh.at(i).getPosition())+",");

    string_to_save_yaml.append(std::to_string(m_jh.back().getPosition())+"]\" ");
    
    if (system( string_to_save_yaml.c_str() )!=0)
      ROS_WARN("Unable to store yaml parameters");
  

    //Save on a .YAML file
    string_to_save_yaml =   "rosparam dump "+m_path_to_config+"/"+msg->value+".yaml /teach_teleop_info_position";
  
    if (system( string_to_save_yaml.c_str() )!=0)
      ROS_WARN("Unable to store yaml parameters");  
  }
  
  
  
  if (!strcmp(msg->key.c_str(), "Move"))
  {
    //Load from selected .YAML to a parameter
    std::string string_to_save_yaml =   "rosparam load "+m_path_to_config+"/"+msg->value+".yaml /teach_teleop_info_position"; 
    if (system( string_to_save_yaml.c_str() )!=0)
      ROS_WARN("Unable to store yaml parameters");  
    
    //Update m_target_pos
    if (!m_nh.getParam("/teach_teleop_info_position/target_pos", m_target_pos))
    {
      ROS_FATAL_STREAM("Parameter '/teach_teleop_info_position/target_pos' does not exist");
      return false;
    }
    m_move = true;
  }
  
  
  
  if (!strcmp(msg->key.c_str(), "ReleaseMove"))
  {
    m_move = false;
  }
  
  
  if (!strcmp(msg->key.c_str(), "Delete"))
  {
    //Delete the parameter
    std::string string_to_save_yaml =   "rosparam delete /teach_teleop_info_position"; 
    if (system( string_to_save_yaml.c_str() )!=0)
      ROS_WARN("Unable to delete parameter /teach_teleop_info_position");  
    
    //Delete .YAML file
    string_to_save_yaml =   "rm "+m_path_to_config+"/"+msg->value+".yaml";
  
    if (system( string_to_save_yaml.c_str() )!=0)
      ROS_WARN("Unable to delete .YAML file");  
  }

  
  return true;
    
    
}

}
}

