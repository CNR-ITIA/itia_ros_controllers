#ifndef itia_joint_teleop_controller__20188101642
#define itia_joint_teleop_controller__20188101642

# include <ros/ros.h>
# include <ros/callback_queue.h>

# include <controller_interface/controller.h>
# include <hardware_interface/joint_command_interface.h>
# include <itia_basic_hardware_interface/posveleff_command_interface.h>
# include <sensor_msgs/JointState.h>

# include <subscription_notifier/subscription_notifier.h>
# include <eigen_state_space_systems/eigen_state_space_systems.h>
# include <eigen_state_space_systems/eigen_controllers.h>

#include <urdf_model/model.h>
#include <urdf_parser/urdf_parser.h>

#include <name_sorting/name_sorting.h>
// #include <joint_teleop_gui/joint_teleop_gui.h>


namespace itia
{
namespace control
{
class JointTeleopController: public controller_interface::Controller<hardware_interface::PosVelEffJointInterface>
{

public:
    JointTeleopController();
    bool init ( hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh );
    void update ( const ros::Time& time, const ros::Duration& period );
    void starting ( const ros::Time& time );
    void stopping ( const ros::Time& time );
    void callback(const sensor_msgs::JointStateConstPtr msg);
    void delta();
    
protected:
    hardware_interface::PosVelEffJointInterface* m_hw;
    std::vector<hardware_interface::PosVelEffJointHandle> m_jh;
    
    ros::NodeHandle m_nh;
    ros::NodeHandle m_root_nh;
    ros::CallbackQueue m_queue;

    ros::NodeHandle m_controller_nh;
    std::vector<std::string> m_joint_names;
    unsigned int m_nAx;

    std::vector<double> m_last_target_vel;
    std::vector<double> m_target_vel;
    std::vector<double> m_target_pos;
    std::vector<double> m_cmd_pos;
    std::vector<double> m_err_pos_delta;
    double m_err_delta;
    std::vector<double> m_saturated_vel;
    
    std::vector<double> m_upper_limit;
    std::vector<double> m_lower_limit;
    std::vector<double> m_velocity_limit;
    std::vector<double> m_acceleration_limit;

    double m_time;
    bool m_configured;
    bool m_compared;
    bool m_check1 = false;
    bool m_check2 = false;
    
    urdf::ModelInterfaceSharedPtr m_model;
    
    double m_err;
    double m_err_old = 0;
    

    std::shared_ptr<ros_helper::SubscriptionNotifier<sensor_msgs::JointState>> m_joint_target_rec;






};
}
}







#endif
