cmake_minimum_required(VERSION 2.8.3)
project(itia_joint_teleop_controller)
add_compile_options(-std=c++11)


find_package(catkin REQUIRED COMPONENTS
  urdf
  controller_interface
  controller_manager
  eigen_state_space_systems
  hardware_interface
  pluginlib
  roscpp
  sensor_msgs
  std_msgs
  subscription_notifier
  itia_basic_hardware_interface
  name_sorting
)
find_package(Eigen3 3.3 REQUIRED NO_MODULE)


catkin_package(
 INCLUDE_DIRS include
 LIBRARIES itia_joint_teleop_controller
 CATKIN_DEPENDS urdf controller_interface controller_manager eigen_state_space_systems hardware_interface pluginlib roscpp sensor_msgs std_msgs subscription_notifier itia_basic_hardware_interface name_sorting
 DEPENDS
)

include_directories(
src
include
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIRS}
)
link_directories(${catkin_LIBRARY_DIRS})

add_library(${PROJECT_NAME}
  src/${PROJECT_NAME}/itia_joint_teleop_controller.cpp
)


add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
)
