﻿#ifndef __itia_cart_impedance__
#define __itia_cart_impedance__

# include <ros/ros.h>
# include <ros/callback_queue.h>
# include <std_srvs/SetBool.h>
# include <itia_basic_hardware_interface/posveleff_command_interface.h>
# include <controller_interface/controller.h>
# include <subscription_notifier/subscription_notifier.h>
# include <sensor_msgs/JointState.h>
# include <geometry_msgs/WrenchStamped.h>
# include <std_msgs/Float64MultiArray.h>
# include <rosdyn_core/primitives.h>
# include <name_sorting/name_sorting.h>

namespace itia
{
  namespace control
  {
    class CartImpedanceController : public controller_interface::Controller<hardware_interface::PosVelEffJointInterface>
    {
    public:
      bool init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
      void update(const ros::Time& time, const ros::Duration& period);
      void starting(const ros::Time& time);
      void stopping(const ros::Time& time);
     
    protected:
      hardware_interface::PosVelEffJointInterface* m_hw;
      std::vector<hardware_interface::PosVelEffJointHandle> m_joint_handles;
      ros::NodeHandle m_root_nh;
      ros::NodeHandle m_controller_nh;
      ros::CallbackQueue m_queue;

      unsigned int m_nAx;
      std::vector< std::string > m_joint_names;
      std::string m_base_frame;
      std::string m_tool_frame;
      std::string m_sensor_frame;
      bool m_base_is_reference;
      boost::shared_ptr<rosdyn::Chain> m_chain_bt;
      boost::shared_ptr<rosdyn::Chain> m_chain_bs;
      Eigen::Vector3d grav;

      
      Eigen::VectorXd m_target;
      Eigen::VectorXd m_Dtarget;
      
      Eigen::VectorXd m_x;
      Eigen::VectorXd m_Dx;
      Eigen::VectorXd m_DDx;

      Eigen::VectorXd m_velocity_limits;
      Eigen::VectorXd m_acceleration_limits;
      Eigen::VectorXd m_effort_limits;
      Eigen::VectorXd m_upper_limits;
      Eigen::VectorXd m_lower_limits;
      double m_cart_velocity_limit;
      double m_cart_acceleration_limit;
      double m_cart_force_limit;

      Eigen::Vector6d m_Jinv;
      Eigen::Vector6d m_Jinv_init;
      Eigen::VectorXd m_damping;
      Eigen::VectorXd m_damping_init;
      Eigen::VectorXd m_k;
      Eigen::VectorXd m_k_init;
      Eigen::Vector6d m_wrench_deadband;
      Eigen::VectorXd m_DDq_deadband;
      
      Eigen::Vector6d m_wrench_of_tool_in_base_with_deadband;
      Eigen::Vector6d m_wrench_of_sensor_in_sensor;
      Eigen::Affine3d m_T_tool_sensor;
      Eigen::Affine3d m_T_base_tool;

      double m_vel_norm;
      double m_vel_old;
      double m_acc_norm;
      double m_force_norm;
      Eigen::Vector6d m_err_norm;
      std::vector<double> m_scale;

      bool m_is_configured;
      bool m_target_ok;
      bool m_effort_ok;
      bool m_cartesian_limits_ok;
      
      std::shared_ptr<ros_helper::SubscriptionNotifier<sensor_msgs::JointState>> m_target_sub;
      std::shared_ptr<ros_helper::SubscriptionNotifier<geometry_msgs::WrenchStamped>> m_wrench_sub;
      std::shared_ptr<ros_helper::SubscriptionNotifier<std_msgs::Float64MultiArray>> m_scaling_in_sub;

      ros::Publisher m_scaling_out_pub;
      std_msgs::Float64MultiArray m_scaling_msg_pub;
      ros::ServiceClient srv_client;


     
      void setTargetCallback(const sensor_msgs::JointStateConstPtr& msg);
      void setWrenchCallback(const geometry_msgs::WrenchStampedConstPtr& msg);
      void scalingCallback(const std_msgs::Float64MultiArrayConstPtr& msg);
      
      ~CartImpedanceController();
      
    };
    
    
  }
}


#endif
