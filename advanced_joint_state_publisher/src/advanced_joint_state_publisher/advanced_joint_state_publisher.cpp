#include <advanced_joint_state_publisher/advanced_joint_state_publisher.h>
#include <pluginlib/class_list_macros.h> // header for PLUGINLIB_EXPORT_CLASS. NOTE IT SHOULD STAY IN THE CPP FILE NOTE 
PLUGINLIB_EXPORT_CLASS(ros::control::JointStatePublisher, controller_interface::ControllerBase);

namespace ros
{
  namespace control
  {
    
    bool JointStatePublisher::init ( hardware_interface::JointStateInterface* hw,
                                     ros::NodeHandle& root_nh,
                                     ros::NodeHandle& controller_nh )
    {
      /* called immediately after the constructor */
      m_hw            = hw;
      m_root_nh       = root_nh;
      m_controller_nh = controller_nh;
      m_controller_nh.setCallbackQueue(&m_queue);
      if (!m_controller_nh.getParam("controlled_joint",m_joint_names))
      {
        ROS_INFO("Parameter control joint names is not defined, publish all joint");
        m_joint_names=m_hw->getNames();
      }
      
      m_hw->getNames();
      m_nax=m_joint_names.size();

      for (std::string name: m_joint_names)
      {
        try 
        {
          m_hw->getHandle(name);
        }
        catch(...)
        {
          ROS_ERROR("joint named %s is not managed by hardware_interface",name.c_str());
          return false;
        }
      }
      
      m_pub =  m_controller_nh.advertise<sensor_msgs::JointState>("joint_states",1);

      m_queue.callAvailable();
      return true;
    }
    
    void JointStatePublisher::starting ( const ros::Time& time )
    {
      m_queue.callAvailable();
    }
    
    void JointStatePublisher::update ( const ros::Time& time, const ros::Duration& period )
    {
      m_queue.callAvailable();

      sensor_msgs::JointStatePtr msg(new sensor_msgs::JointState());
      msg->position.resize(m_nax,0);
      msg->velocity.resize(m_nax,0);
      msg->effort.resize(m_nax,0);
      msg->name=m_joint_names;
      
      for (std::size_t idx=0;idx<m_nax;idx++)
      {
        msg->position.at(idx)=m_hw->getHandle(m_joint_names.at(idx)).getPosition();
        msg->velocity.at(idx)=m_hw->getHandle(m_joint_names.at(idx)).getVelocity();
        msg->effort.at(idx)  =m_hw->getHandle(m_joint_names.at(idx)).getEffort();
      }
      msg->header.stamp=ros::Time::now();
      m_pub.publish(msg);
    }
    
    void JointStatePublisher::stopping ( const ros::Time& time )
    {
      m_pub.shutdown();
    }
    
    
    
  }
}
