#ifndef JointStatePublisher_201809210739
#define JointStatePublisher_201809210739

#include <ros/callback_queue.h>
#include <controller_interface/controller.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <sensor_msgs/JointState.h>
#include <ros/ros.h>
namespace ros
{
namespace control
{
  
  class JointStatePublisher: public controller_interface::Controller<hardware_interface::JointStateInterface>
  {
  public:
    virtual bool init(hardware_interface::JointStateInterface* hw,
                      ros::NodeHandle&                         root_nh,
                      ros::NodeHandle&                         controller_nh);
    virtual void starting(const ros::Time& time);
    virtual void update(const ros::Time& time, const ros::Duration& period);
    virtual void stopping(const ros::Time& time);
    
  protected:
    hardware_interface::JointStateInterface* m_hw;
    ros::NodeHandle m_root_nh;
    ros::NodeHandle m_controller_nh;
    ros::CallbackQueue m_queue;
    std::vector<std::string> m_joint_names;
    
    ros::Publisher m_pub;
    std::size_t m_nax;
  };
}
}


#endif
