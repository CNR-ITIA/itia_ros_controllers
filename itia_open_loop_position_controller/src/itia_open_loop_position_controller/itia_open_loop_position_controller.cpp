#include <itia_open_loop_position_controller/itia_open_loop_position_controller.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(itia::control::OpenLoopPositionController, controller_interface::ControllerBase);


namespace itia
{
namespace control
{


bool OpenLoopPositionController::init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  m_hw=hw;
  if (!controller_nh.getParam("controlled_joint",m_joint_name))
  {
    ROS_FATAL("controlled_joint is not defined!");
    return false;
  }

  bool flag=false;
  for (unsigned idx=0;idx<m_hw->getNames().size();idx++)
  {
    if (!m_hw->getNames().at(idx).compare(m_joint_name))
    {
      m_jh=m_hw->getHandle(m_joint_name);
      flag=true;
      break;
    }
  }
  if (!flag)
  {
    ROS_FATAL("%s is not managed by %s",m_joint_name.c_str(),m_root_nh.getNamespace().c_str());
    return false;
  }

  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_controller_nh.setCallbackQueue(&m_queue);


  if (!m_controller_nh.getParam("setpoint_topic_name", setpoint_topic_name))
  {
    ROS_ERROR_STREAM(m_controller_nh.getNamespace()+"/'setpoint_topic_name' does not exist");
    ROS_ERROR("ERROR DURING INITIALIZATION CONTROLLER '%s'", m_controller_nh.getNamespace().c_str());
    return false;
  }

  m_target_js_rec.reset(new ros_helper::SubscriptionNotifier<sensor_msgs::JointState>(m_controller_nh,setpoint_topic_name, 1,boost::bind(&OpenLoopPositionController::callback,this,_1)));

  ROS_DEBUG("Controller '%s' controls the following joint: %s",m_controller_nh.getNamespace().c_str(),m_joint_name.c_str());
  ROS_INFO("Controller '%s' well initialized",m_controller_nh.getNamespace().c_str());

  return true;
}

void OpenLoopPositionController::starting(const ros::Time& time)
{
  ROS_INFO("[ %s ] Starting controller",  m_controller_nh.getNamespace().c_str());
  m_pos_cmd = m_jh.getPosition();
}

void OpenLoopPositionController::stopping(const ros::Time& time)
{
  ROS_INFO("[ %s ] Stopping controller",  m_controller_nh.getNamespace().c_str());
}

void OpenLoopPositionController::update(const ros::Time& time, const ros::Duration& period)
{
  try
  {
    m_queue.callAvailable();
    m_jh.setCommand(m_pos_cmd);
  }
  catch (...)
  {
    ROS_WARN("something wrong: Controller '%s'",m_controller_nh.getNamespace().c_str());
    m_jh.setCommand(m_jh.getPosition());
  }
}

bool OpenLoopPositionController::extractJoint(const sensor_msgs::JointState msg, const std::string name, double& pos)
{
  for (unsigned int iJoint=0;iJoint<msg.name.size();iJoint++)
  {
    if (!msg.name.at(iJoint).compare(name))
    {
      if (msg.position.size()>(iJoint ))
        pos=msg.position.at(iJoint);
      else
      {
        ROS_ERROR_THROTTLE(0.5,"message has no position field for joint %s",name.c_str());
        return false;
      }
      return true;
    }
  }
  return false;
}

void OpenLoopPositionController::callback(const sensor_msgs::JointStateConstPtr msg)
{
  if (!extractJoint(*msg,m_joint_name,m_pos_cmd))
  {
    ROS_FATAL_STREAM(m_controller_nh.getNamespace()+" target message dimension is wrong");
  }
  return;
}


}
}
