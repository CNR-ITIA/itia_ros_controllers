#ifndef itia_open_loop_position_controller__20190204
#define itia_open_loop_position_controller__20190204

# include <controller_interface/controller.h>
# include <hardware_interface/joint_command_interface.h>

# include <thread>
# include <mutex>
# include <boost/graph/graph_concepts.hpp>
# include <ros/ros.h>
# include <sensor_msgs/JointState.h>
# include <pluginlib/class_list_macros.h>

# include <subscription_notifier/subscription_notifier.h>
# include <ros/callback_queue.h>


namespace itia
{
  namespace control
  {


    class OpenLoopPositionController : public controller_interface::Controller<hardware_interface::PositionJointInterface>
    {
    public:
      bool init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
      void update(const ros::Time& time, const ros::Duration& period);
      void starting(const ros::Time& time);
      void stopping(const ros::Time& time);


      std::string getJointName()
      {
        return m_joint_name;
      }

    protected:

      hardware_interface::PositionJointInterface* m_hw;
      hardware_interface::JointHandle m_jh;

      ros::CallbackQueue m_queue;
      boost::shared_ptr<ros::AsyncSpinner> m_spinner;

      std::string setpoint_topic_name;

      std::string m_joint_name;
      double m_pos_cmd;

      std::shared_ptr<ros_helper::SubscriptionNotifier<sensor_msgs::JointState>> m_target_js_rec;

      ros::NodeHandle m_root_nh;
      ros::NodeHandle m_controller_nh;

      void callback(const sensor_msgs::JointStateConstPtr msg);
      bool extractJoint(const sensor_msgs::JointState msg, const std::string name, double& vel);

    };


  }
}

# endif
