#include <itia_joint_impedance_controller/itia_joint_impedance_controller.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(itia::control::JointImpedanceController, controller_interface::ControllerBase);


namespace itia
{
namespace control
{
    
    
JointImpedanceController::~JointImpedanceController()
{
  
}

bool JointImpedanceController::init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{

  ROS_INFO("[ %s ] init controller",  m_controller_nh.getNamespace().c_str());
  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_hw = hw;
  
  m_controller_nh.setCallbackQueue(&m_queue);
  
  try
  {
    
    m_target_ok=false;
    m_effort_ok=false;

    std::string joint_target = "joint_target";
    std::string external_torques = "external_torques";
    std::string external_wrench = "external_wrench";

    if (!m_controller_nh.getParam("joint_target_topic", joint_target))
    {
      ROS_WARN_STREAM(m_controller_nh.getNamespace()+"/'joint_target' does not exist. Default value 'joint_target' superimposed.");
      joint_target = "joint_target";
    }

    ROS_INFO("subscribing %s",joint_target.c_str());
    m_target_sub.reset(new ros_helper::SubscriptionNotifier<sensor_msgs::JointState>(m_controller_nh,joint_target,1));
    m_target_sub->setAdvancedCallback(boost::bind(&itia::control::JointImpedanceController::setTargetCallback,this,_1));

    if (!m_controller_nh.getParam("use_wrench", m_use_wrench))
    {
      ROS_WARN_STREAM(m_controller_nh.getNamespace()+"/'use_wrench' does not exist. Default value false.");
      m_use_wrench = false;
    }

    if (m_use_wrench)
    {

      std::string robot_description;
      if (!m_root_nh.getParam("/robot_description", robot_description))
      {
        ROS_FATAL_STREAM("Parameter '/robot_description' does not exist");
        return false;
      }

      urdf::Model urdf_model;
      urdf_model.initParam("robot_description");

      if (!m_controller_nh.getParam("base_frame",m_base_frame))
      {
        ROS_ERROR("base_link not defined");
        return 0;
      }
      if (!m_controller_nh.getParam("tool_frame",m_tool_frame))
      {
        ROS_ERROR("tool_link not defined");
        return 0;
      }
      if (!m_controller_nh.getParam("sensor_frame",m_sensor_frame))
      {
        ROS_ERROR("sensor_frame not defined");
        return 0;
      }


      Eigen::Vector3d gravity;
      gravity << 0, 0, -9.806;
      m_chain_bt = rosdyn::createChain(urdf_model,m_base_frame,m_tool_frame,gravity);
      m_chain_bs = rosdyn::createChain(urdf_model,m_base_frame,m_sensor_frame,gravity);
      m_wrench_sub.reset(new ros_helper::SubscriptionNotifier<geometry_msgs::WrenchStamped>(m_controller_nh,external_wrench,1));
      m_wrench_sub->setAdvancedCallback(boost::bind(&itia::control::JointImpedanceController::setWrenchCallback,this,_1));
    }
    else
    {
      if (!m_controller_nh.getParam("external_torques_topic", external_torques ))
      {
        ROS_WARN_STREAM(m_controller_nh.getNamespace()+"/'external_torques' does not exist. Default value 'external_torques' superimposed.");
        external_torques = "external_torques";
      }
      m_effort_sub.reset(new ros_helper::SubscriptionNotifier<sensor_msgs::JointState>(m_controller_nh,external_torques,1));
      m_effort_sub->setAdvancedCallback(boost::bind(&itia::control::JointImpedanceController::setEffortCallback,this,_1));
    }




    m_adapt_params.reset(new ros_helper::SubscriptionNotifier<sensor_msgs::JointState>(m_controller_nh,"/ur5_32/joint_states", 1,boost::bind(&JointImpedanceController::adaptCallback,this,_1)));





    

    if (!controller_nh.getParam("controlled_joint",m_joint_names))
    {
      ROS_FATAL("controlled_joint not defined");
      return false;
    }
    m_nax=m_joint_names.size();
    
    
    m_joint_handles.resize(m_nax);
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      m_joint_handles.at(iAx)=m_hw->getHandle(m_joint_names.at(iAx));
    }

    m_target.resize(m_nax);
    m_Dtarget.resize(m_nax);
    m_x.resize(m_nax);
    m_Dx.resize(m_nax);
    m_DDx.resize(m_nax);
    
    m_Jinv.resize(m_nax);
    m_damping.resize(m_nax);
    m_damping_dafault.resize(m_nax);
    m_k.resize(m_nax);
    m_k_default.resize(m_nax);
    m_k_new.resize(m_nax);
    m_torque_deadband.resize(m_nax);
    m_torque.resize(m_nax);
    
    m_target.setZero();
    m_Dtarget.setZero();
    m_x.setZero();
    m_Dx.setZero();
    m_DDx.setZero();
    m_torque.setZero();
    



    std::vector<double> inertia, damping, stiffness, torque_deadband;
    if (!m_controller_nh.getParam("inertia", inertia))
    {
      ROS_FATAL_STREAM(m_controller_nh.getNamespace()+"/'inertia' does not exist");
      ROS_FATAL("ERROR DURING INITIALIZATION CONTROLLER '%s'", m_controller_nh.getNamespace().c_str());
      return false;
    }
    if (!m_controller_nh.getParam("damping", damping))
    {
      ROS_FATAL_STREAM(m_controller_nh.getNamespace()+"/'damping' does not exist");
      ROS_FATAL("ERROR DURING INITIALIZATION CONTROLLER '%s'", m_controller_nh.getNamespace().c_str());
      return false;
    }
    if (!m_controller_nh.getParam("stiffness", stiffness))
    {
      ROS_FATAL_STREAM(m_controller_nh.getNamespace()+"/'stiffness' does not exist");
      ROS_FATAL("ERROR DURING INITIALIZATION CONTROLLER '%s'", m_controller_nh.getNamespace().c_str());
      return false;
    }
    if (!m_controller_nh.getParam("torque_deadband", torque_deadband))
    {
      ROS_FATAL_STREAM(m_controller_nh.getNamespace()+"/'torque_deadband' does not exist");
      ROS_FATAL("ERROR DURING INITIALIZATION CONTROLLER '%s'", m_controller_nh.getNamespace().c_str());
      return false;
    }
    
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      if (inertia.at(iAx)<=0)
      {
        ROS_INFO("inertia value of Joint %d is not positive, disabling impedance control for this axis",iAx);
        m_Jinv(iAx)=0.0;
      }
      else
        m_Jinv(iAx)=1.0/inertia.at(iAx);
      
      if (damping.at(iAx)<=0)
      {
        ROS_INFO("damping value of Joint %d is not positive, setting equalt to 10/inertia",iAx);
        m_damping(iAx)=10.0*m_Jinv(iAx);
        m_damping_dafault(iAx)=10.0*m_Jinv(iAx);
      }
      else
      {
        m_damping(iAx)=damping.at(iAx);
        m_damping_dafault(iAx)=damping.at(iAx);
      }
      
      
      if (stiffness.at(iAx)<0)
      {
        ROS_INFO("maximum fitness value of Joint %d is negative, setting equal to 0",iAx);
        m_k(iAx)=0.0;
        m_k_default(iAx)=0.0;
      }
      else
      {
        m_k(iAx)=stiffness.at(iAx);
        m_k_default(iAx)=stiffness.at(iAx);
      }

      if (torque_deadband.at(iAx)<=0)
      {
        ROS_INFO("torque_deadband value of Joint %d is not positive, disabling impedance control for this axis",iAx);
        m_torque_deadband(iAx)=0.0;
      }
      else
        m_torque_deadband(iAx)=torque_deadband.at(iAx);
      
    }
    
  }
  catch(const  std::exception& e)
  {
    ROS_FATAL("EXCEPTION: %s", e.what());
    return false;
  }
  ROS_INFO("[ %s ] init OK controller",  m_controller_nh.getNamespace().c_str());

  return true;
  
}

void JointImpedanceController::starting(const ros::Time& time)
{
   ROS_INFO("[ %s ] Starting controller",  m_controller_nh.getNamespace().c_str());

  m_queue.callAvailable();
  m_is_configured=m_target_ok && m_effort_ok;
  if (m_is_configured)
  {
    ROS_INFO("Joint Impedance Controller Configured");
  }
  for (unsigned int iAx=0;iAx<m_nax;iAx++)
  {
    m_x(iAx)=m_joint_handles.at(iAx).getPosition();
    m_Dx(iAx)=m_joint_handles.at(iAx).getVelocity();

    ROS_INFO("iAx=%u, x=%f, Dx=%f",iAx,m_x(iAx),m_Dx(iAx));
  }
  ROS_INFO("[ %s ] Started controller",  m_controller_nh.getNamespace().c_str());
}

void JointImpedanceController::stopping(const ros::Time& time)
{

  for (unsigned int iAx=0;iAx<m_nax;iAx++)
  {
    m_joint_handles.at(iAx).setCommand(m_x(iAx),0.0,0.0);
  }




}

void JointImpedanceController::update(const ros::Time& time, const ros::Duration& period)
{
  m_queue.callAvailable();
  

  if (m_is_configured)
  {

    
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      if ( m_torque(iAx) > m_torque_deadband(iAx) )
        m_torque(iAx)-=m_torque_deadband(iAx);
      else if ( m_torque(iAx) < -m_torque_deadband(iAx) )
        m_torque(iAx)+=m_torque_deadband(iAx);
      else
        m_torque(iAx)=0.0;
    }
    
    
    
    m_DDx = m_Jinv.cwiseProduct( m_k.cwiseProduct(m_target-m_x) + m_damping.cwiseProduct(m_Dtarget-m_Dx) + m_torque );
    m_x  += m_Dx  * period.toSec() + m_DDx*std::pow(period.toSec(),2.0)*0.5;
    m_Dx += m_DDx * period.toSec();
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      m_joint_handles.at(iAx).setCommand(m_x(iAx),m_Dx(iAx),0.0);
    }
    
  }
  else
  {
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
      m_joint_handles.at(iAx).setCommand(m_x(iAx),0.0,0.0);
    m_is_configured=m_target_ok && m_effort_ok;
    if (m_is_configured)
    {
      ROS_INFO("configured");
    }
  }
  
}

void JointImpedanceController::setTargetCallback(const sensor_msgs::JointStateConstPtr& msg)
{
  try 
  {
    sensor_msgs::JointState tmp_msg=*msg;
    if (!name_sorting::permutationName(m_joint_names,tmp_msg.name,tmp_msg.position,tmp_msg.velocity,tmp_msg.effort))
    {
      ROS_ERROR("joints not found");
      m_target_ok=false;
      return;
    }
    m_target_ok=true;
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      m_target(iAx)=tmp_msg.position.at(iAx);
      m_Dtarget(iAx)=tmp_msg.velocity.at(iAx);
    }
  }
  catch(...)
  {
    ROS_ERROR("something wrong in target callback");
    m_target_ok=false;
  }
}
void JointImpedanceController::setEffortCallback(const sensor_msgs::JointStateConstPtr& msg)
{

  try
  {

    sensor_msgs::JointState tmp_msg=*msg;
    if (!name_sorting::permutationName(m_joint_names,tmp_msg.name,tmp_msg.effort))
    {
      ROS_ERROR("joints not found");
      m_effort_ok=false;
      return;
    }
    m_effort_ok=true;
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      m_torque(iAx)=tmp_msg.effort.at(iAx);
    }
  }
  catch(...)
  {
    ROS_ERROR("something wrong in target callback");
    m_effort_ok=false;
  }
}

void JointImpedanceController::setWrenchCallback(const geometry_msgs::WrenchStampedConstPtr& msg)
{

  if (msg->header.frame_id.compare(m_sensor_frame))
  {
    ROS_WARN("sensor frame is %s, it should be %s",msg->header.frame_id.c_str(),m_sensor_frame.c_str());
    return;
  }

  Eigen::Vector6d wrench_of_sensor_in_sensor;
  wrench_of_sensor_in_sensor(0)=msg->wrench.force.x;
  wrench_of_sensor_in_sensor(1)=msg->wrench.force.y;
  wrench_of_sensor_in_sensor(2)=msg->wrench.force.z;
  wrench_of_sensor_in_sensor(3)=msg->wrench.torque.x;
  wrench_of_sensor_in_sensor(4)=msg->wrench.torque.y;
  wrench_of_sensor_in_sensor(5)=msg->wrench.torque.z;

  Eigen::Affine3d T_base_tool=m_chain_bt->getTransformation(m_x);
  Eigen::MatrixXd jacobian_of_tool_in_base = m_chain_bt->getJacobian(m_x);
  Eigen::Affine3d T_base_sensor=m_chain_bs->getTransformation(m_x);
  Eigen::Affine3d T_tool_sensor= T_base_tool.inverse()*T_base_sensor;

  Eigen::Vector6d wrench_of_tool_in_tool = rosdyn::spatialDualTranformation(wrench_of_sensor_in_sensor,T_tool_sensor);
  Eigen::Vector6d wrench_of_tool_in_base = rosdyn::spatialRotation(wrench_of_tool_in_tool,T_base_tool.linear());

  m_torque=jacobian_of_tool_in_base.transpose()*wrench_of_tool_in_base;
  m_effort_ok=true;
}

void JointImpedanceController::adaptCallback(const sensor_msgs::JointStateConstPtr& msg)
{

  for (unsigned int iAx=0;iAx<m_nax;iAx++)
  {
    double vel_ratio = 1-10*abs(msg->velocity.at(iAx)*msg->velocity.at(iAx));

    if (vel_ratio < 0.1)
      vel_ratio = 0.1;

//    if (abs(msg->velocity.at(iAx)) > 0.01)
//    {
//      m_k_new(iAx) = m_k_new(iAx) + m_k_default(iAx)*vel_ratio;

//      m_k(iAx)= m_k_new(iAx) / m_cnt;

//      m_cnt++;
//    }

//    if (abs(msg->velocity.at(iAx)) > 0.01)
//    {
//      m_k(iAx)= m_k_default(iAx)*(vel_ratio);
//      m_damping(iAx) = m_damping_dafault(iAx)*(vel_ratio);
//    }

//    m_k(iAx)= m_k_default(iAx)*(vel_ratio*vel_ratio);




//    m_damping(iAx) = m_damping_dafault(iAx)*vel_ratio;
//    std::cout << "m_k=" << m_k(1) << " ,m_k_default=" << m_k_default(1) << std::endl;

  }



}




}
}
