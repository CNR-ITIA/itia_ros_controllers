#ifndef itia_inertia_matrix_velocity_to_torque_controller__
#define itia_inertia_matrix_velocity_to_torque_controller__



# include <controller_interface/controller.h>
# include <hardware_interface/joint_command_interface.h>
# include <eigen_state_space_systems/eigen_controllers.h>
# include <rosdyn_core/primitives.h>
# include <thread>
# include <mutex>
# include <boost/graph/graph_concepts.hpp>
# include <ros/ros.h>
# include <subscription_notifier/subscription_notifier.h>
# include <sensor_msgs/JointState.h>
# include <pluginlib/class_list_macros.h>
# include <name_sorting/name_sorting.h>
# include <ros/callback_queue.h>

namespace itia
{
  namespace control
  {
    
    class InertiaMatrixVelocityToTorqueController : public controller_interface::Controller<hardware_interface::EffortJointInterface>
    {
    public:
      bool init(hardware_interface::EffortJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
      void update(const ros::Time& time, const ros::Duration& period);
      void starting(const ros::Time& time);
      void stopping(const ros::Time& time);

    protected:
      
      hardware_interface::EffortJointInterface* m_hw;
      

      void setTargetCallback(const sensor_msgs::JointStateConstPtr& msg);

      rosdyn::ChainPtr m_chain;
      std::string m_base_frame;
      std::string m_tool_frame;
      ros::CallbackQueue m_queue;

      std::vector<hardware_interface::JointHandle> m_joint_handles;

      std::vector<std::string> m_joint_names;
      Eigen::VectorXd m_position;
      Eigen::VectorXd m_velocity;
      Eigen::VectorXd m_acceleration;
      Eigen::VectorXd m_effort;

      Eigen::VectorXd m_target_position;
      Eigen::VectorXd m_target_velocity;
      Eigen::VectorXd m_target_effort;
      Eigen::VectorXd m_robust_term;

      double m_Kp;
      double m_Kd;
      double m_rho; //  robustness gain
      bool m_use_target_torque;

      std::vector<double> m_max_effort;
      std::vector<double> m_max_velocity;
      std::shared_ptr<ros_helper::SubscriptionNotifier<sensor_msgs::JointState>> m_target_js_rec;
      
      ros::Subscriber  m_js_target_sub;
      ros::NodeHandle m_root_nh;
      ros::NodeHandle m_controller_nh;
      std::size_t m_nax;
      bool m_configured;
    };
    
    
  }
}






#endif
