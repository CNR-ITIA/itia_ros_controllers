#include <itia_inertia_matrix_velocity_to_torque_controller/itia_inertia_matrix_velocity_to_torque_controller.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(itia::control::InertiaMatrixVelocityToTorqueController, controller_interface::ControllerBase);


namespace itia
{
namespace control
{

bool InertiaMatrixVelocityToTorqueController::init(hardware_interface::EffortJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{

  m_hw=hw;
  m_controller_nh=controller_nh;
  m_root_nh=root_nh;

  // Loading chain
  if (!m_controller_nh.getParam("base_frame",m_base_frame))
  {
    ROS_ERROR("%s/base_link not defined",m_controller_nh.getNamespace().c_str());
    return false;
  }
  if (!m_controller_nh.getParam("tool_frame",m_tool_frame))
  {
    ROS_ERROR("tool_link not defined");
    return false;
  }
  urdf::Model urdf_model;
  if (!urdf_model.initParam("/robot_description"))
  {
    ROS_ERROR("Urdf robot_description '%s' does not exist",(m_controller_nh.getNamespace()+"/robot_description").c_str());
    return false;
  }
  Eigen::Vector3d gravity;
  gravity << 0, 0, -9.806;

  m_chain = rosdyn::createChain(urdf_model,m_base_frame,m_tool_frame,gravity);
  m_joint_names=m_chain->getMoveableJointNames();

  // check joint names
  bool flag=false;
  for (std::string joint_name: m_joint_names)
  {
    for (unsigned idx=0;idx<m_hw->getNames().size();idx++)
    {
      if (!m_hw->getNames().at(idx).compare(joint_name))
      {
        m_joint_handles.push_back(m_hw->getHandle(joint_name));
        ROS_DEBUG("ADD %s handle",joint_name.c_str());
        flag=true;
        break;
      }
    }
    if (!flag)
    {
      ROS_FATAL("Joint %s is not managed",joint_name.c_str());
      return false;
    }
  }

  // set target topic name
  std::string setpoint_topic_name;
  if (!m_controller_nh.getParam("setpoint_topic_name", setpoint_topic_name))
  {
    ROS_FATAL_STREAM(m_controller_nh.getNamespace()+"/'setpoint_topic_name' does not exist");
    ROS_FATAL("ERROR DURING INITIALIZATION CONTROLLER '%s'", m_controller_nh.getNamespace().c_str());
    return false;
  }
  m_target_js_rec.reset(new ros_helper::SubscriptionNotifier<sensor_msgs::JointState>(m_controller_nh,setpoint_topic_name, 1,boost::bind(&InertiaMatrixVelocityToTorqueController::setTargetCallback,this,_1)));

  ROS_DEBUG("Controller '%s' controls the following joint:",m_controller_nh.getNamespace().c_str());
  for (std::string& name: m_joint_names)
    ROS_DEBUG("- %s",name.c_str());

  m_nax=m_joint_names.size();

  // set natural frequency and damping
  double wn, xi;
  if (!m_controller_nh.getParam("natural_frequency",wn))
  {
    ROS_ERROR("Position gain Kp is not set");
    return false;
  }
  if (!m_controller_nh.getParam("damping",xi))
  {
    ROS_ERROR("Velocity gain Kd is not set");
    return false;
  }
  m_Kp=wn*wn;
  m_Kd=2*xi*wn;

  if (!m_controller_nh.getParam("robustness_gain",m_rho))
  {
    ROS_ERROR("robustness_gain is not set");
    return false;
  }

  if (!m_controller_nh.getParam("use_target_torque",m_use_target_torque))
  {
    ROS_DEBUG("use_target_torque is not set, set it off");
    m_use_target_torque=false;
  }

  m_max_effort.resize(m_nax);
  if (!m_controller_nh.getParam("maximum_torque",m_max_effort))
  {
    ROS_DEBUG("no maximum_torque specified, reading from urdf");
    for (unsigned int iAx=0; iAx<m_nax; iAx++)
    {
      m_max_effort.at(iAx) = urdf_model.getJoint(m_joint_names.at(iAx))->limits->effort;
      ROS_DEBUG("joint %s maximum effort = %f", m_joint_names.at(iAx).c_str(),m_max_effort.at(iAx));
    }
  }

  m_max_velocity.resize(m_nax);
  for (unsigned int iAx=0; iAx<m_nax; iAx++)
    m_max_velocity.at(iAx) = urdf_model.getJoint(m_joint_names.at(iAx))->limits->velocity;

  ROS_INFO("Controller '%s' well initialized",m_controller_nh.getNamespace().c_str());
  ROS_DEBUG("Kp=%f, Kd=%f",m_Kp,m_Kd);

  m_acceleration.resize(m_nax);
  m_velocity.resize(m_nax);
  m_position.resize(m_nax);
  m_target_position.resize(m_nax);
  m_target_velocity.resize(m_nax);
  m_effort.resize(m_nax);
  m_target_effort.resize(m_nax);

  m_acceleration.setZero();
  m_velocity.setZero();
  m_position.setZero();
  m_target_position.setZero();
  m_target_velocity.setZero();
  m_effort.setZero();
  m_target_effort.setZero();
  return true;
}

void InertiaMatrixVelocityToTorqueController::starting(const ros::Time& time)
{
  m_configured = false;

  for (unsigned int iax=0;iax<m_nax;iax++)
  {
    m_position(iax) = m_joint_handles.at(iax).getPosition();
    m_velocity(iax) = m_joint_handles.at(iax).getVelocity();
    m_effort(iax)   = m_joint_handles.at(iax).getEffort();
  }
  m_target_position=m_position;
  m_queue.callAvailable();
}

void InertiaMatrixVelocityToTorqueController::stopping(const ros::Time& time)
{
  ROS_INFO("[ %s ] Stopping controller", m_controller_nh.getNamespace().c_str());
  m_configured = false;
}

void InertiaMatrixVelocityToTorqueController::update(const ros::Time& time, const ros::Duration& period)
{

  m_queue.callAvailable();

  for (unsigned int iax=0;iax<m_nax;iax++)
  {
    m_position(iax) = m_joint_handles.at(iax).getPosition();
    m_velocity(iax) = m_joint_handles.at(iax).getVelocity();
    m_velocity(iax) = std::max(-m_max_velocity.at(iax),std::min(m_velocity(iax),m_max_velocity.at(iax)));
    m_effort(iax)   = m_joint_handles.at(iax).getEffort();
  }

  Eigen::VectorXd velocity_error=m_target_velocity-m_velocity;

  double velocity_norm=velocity_error.norm();
  if (velocity_norm<1e-4)
    m_robust_term=m_rho*1e4*velocity_error;
  else
    m_robust_term=m_rho*velocity_error/velocity_norm;

  m_acceleration=m_Kp*(m_target_position-m_position)+m_Kd*velocity_error+m_robust_term;

  m_effort=m_chain->getJointTorque(m_position,m_velocity,m_acceleration);
  if (m_use_target_torque)
    m_effort+=m_target_effort;

  for (unsigned int iax=0;iax<m_nax;iax++)
  {
    m_effort(iax)=std::max(-m_max_effort.at(iax),std::min(m_effort(iax),m_max_effort.at(iax)));
    m_joint_handles.at(iax).setCommand(m_effort(iax));
  }
}


void InertiaMatrixVelocityToTorqueController::setTargetCallback(const sensor_msgs::JointStateConstPtr& msg)
{
  try
  {
    sensor_msgs::JointState tmp_msg=*msg;
    if (!name_sorting::permutationName(m_joint_names,tmp_msg.name,tmp_msg.position,tmp_msg.velocity,tmp_msg.effort))
    {
      ROS_ERROR_THROTTLE(1,"joints not found");
      m_configured=false;
      return;
    }
    if (!m_configured)
      ROS_INFO("First target message received");

    m_configured=true;
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      m_target_position(iAx) = tmp_msg.position.at(iAx);
      m_target_velocity(iAx) = tmp_msg.velocity.at(iAx);
      m_target_effort(iAx)   = tmp_msg.effort.at(iAx);
    }
  }
  catch(...)
  {
    ROS_ERROR("something wrong in target callback");
    m_configured=false;
  }
}
}  //  namespace control
}  //  namespace itia
