#include <itia_homing_controller/itia_homing_controller.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(itia::control::HomingController, controller_interface::ControllerBase);


namespace itia
{
namespace control
{
    
bool HomingController::init(hardware_interface::JointStateInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  m_hw=hw;
  if (!controller_nh.getParam("joint_name",m_joint_name))
  {
    ROS_ERROR("No joint_name speficied");
    return false;
  }
  
  bool flag=false;
  for (unsigned idx=0;idx<hw->getNames().size();idx++)
  {
    if (!hw->getNames().at(idx).compare(m_joint_name))
    {
      m_jh=hw->getHandle(m_joint_name);
      hw->claim(m_joint_name);
      
      ROS_FATAL("Claiming");
      flag=true;
      break;
    }
  }
  if (!flag)
  {
    ROS_FATAL("ERROR");
    return false;
  }
  ROS_INFO("Initialized homing controller of joint %s",m_joint_name.c_str());
  return true;
}

void HomingController::starting(const ros::Time& time)
{
  ROS_INFO("Start homing of joint %s",m_joint_name.c_str());
}

void HomingController::stopping(const ros::Time& time)
{
  ROS_INFO("stopping homing of joint %s",m_joint_name.c_str());
  
}

void HomingController::update(const ros::Time& time, const ros::Duration& period)
{
}
    
  }
}
