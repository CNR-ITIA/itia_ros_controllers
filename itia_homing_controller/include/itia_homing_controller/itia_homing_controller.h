#ifndef ITIA_HOMING_CONTROLLER_201806061719
#define ITIA_HOMING_CONTROLLER_201806061719

#include <hardware_interface/joint_state_interface.h>
#include <controller_interface/controller.h>
namespace itia
{
  namespace control
  {
    
    class HomingController : public controller_interface::Controller<hardware_interface::JointStateInterface>
    {
    public:
      bool init(hardware_interface::JointStateInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
      void update(const ros::Time& time, const ros::Duration& period);
      void starting(const ros::Time& time);
      void stopping(const ros::Time& time);
      
      
    protected:
      
      hardware_interface::JointStateInterface* m_hw;
      hardware_interface::JointStateHandle m_jh;
      std::string m_joint_name;
    };
    
  }
}

# endif