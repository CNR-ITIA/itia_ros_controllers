#ifndef robot_state_controller_20190319
#define robot_state_controller_20190319

#include <controller_interface/controller.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/TwistStamped.h>
#include <rosdyn_core/primitives.h>
#include <rosdyn_core/urdf_parser.h>
#include <ros/ros.h>


namespace robot_state_controller
{



  class RobotStateController: public controller_interface::Controller<hardware_interface::JointStateInterface>
  {
  public:
    virtual bool init(hardware_interface::JointStateInterface* hw,
                      ros::NodeHandle&                         root_nh,
                      ros::NodeHandle&                         controller_nh);
    virtual void starting(const ros::Time& time);
    virtual void update(const ros::Time& time, const ros::Duration& period);
    virtual void stopping(const ros::Time& time);

  protected:
    hardware_interface::JointStateInterface* m_hw;
    std::vector<std::string> m_joint_names;
    std::vector<hardware_interface::JointStateHandle> m_jh;

    std::vector<ros::Publisher> m_pubs_in_link;
    std::vector<ros::Publisher> m_pubs_in_base;
    std::size_t m_nax;


    std::string m_base_frame;
    std::string m_tool_frame;
    std::vector<std::string> m_frames;
    std::vector<unsigned int> m_frame_idxs;
    ros::NodeHandle m_root_nh;
    ros::NodeHandle m_controller_nh;

    rosdyn::ChainPtr m_chain;
    Eigen::VectorXd m_q;
    Eigen::VectorXd m_Dq;

  };
}




#endif
