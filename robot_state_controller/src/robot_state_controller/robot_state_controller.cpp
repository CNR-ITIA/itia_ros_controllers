#include <robot_state_controller/robot_state_controller.h>
#include <pluginlib/class_list_macros.h> // header for PLUGINLIB_EXPORT_CLASS. NOTE IT SHOULD STAY IN THE CPP FILE NOTE
PLUGINLIB_EXPORT_CLASS(robot_state_controller::RobotStateController, controller_interface::ControllerBase);

namespace robot_state_controller
{

bool RobotStateController::init ( hardware_interface::JointStateInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh )
{
  /* called immediately after the constructor */
  m_hw=hw;

  m_controller_nh=controller_nh;
  m_root_nh=root_nh;

  if (!controller_nh.getParam("controlled_joint",m_joint_names))
  {
    ROS_FATAL("ERROR");
    return false;
  }

  m_nax=m_joint_names.size();

  for (std::string name: m_joint_names)
  {
    try
    {
      m_jh.push_back(m_hw->getHandle(name));
    }
    catch(...)
    {
      ROS_ERROR("joint named %s is not managed by hardware_interface",name.c_str());
      return false;
    }
  }


  urdf::Model urdf_model;
  if (!urdf_model.initParam("/robot_description"))
  {
    ROS_ERROR("Urdf robot_description '%s' does not exist",(m_controller_nh.getNamespace()+"/robot_description").c_str());
    return false;
  }
  Eigen::Vector3d gravity;
  gravity << 0, 0, -9.806;


  if (!m_controller_nh.getParam("base_frame",m_base_frame))
  {
    ROS_ERROR("base_link not defined");
    return false;
  }
  if (!m_controller_nh.getParam("tool_frame",m_tool_frame))
  {
    ROS_ERROR("tool_link not defined");
    return false;
  }
  if (!m_controller_nh.getParam("frames",m_frames))
  {
    ROS_ERROR("frames not defined");
    return false;
  }

  m_chain = rosdyn::createChain(urdf_model,m_base_frame,m_tool_frame,gravity);
  m_chain->setInputJointsName(m_joint_names);

  std::vector<std::string> link_names=m_chain->getLinksName();

  for (unsigned int idx=0;idx<m_frames.size();idx++)
  {
    bool found=false;
    for (unsigned int il=0;il<link_names.size();il++)
    {
      if (!m_frames.at(idx).compare(link_names.at(il)))
      {
        m_frame_idxs.push_back(il);
        found=true;
      }
    }
    if (!found)
    {
      ROS_ERROR("unable to find frame %s",m_frames.at(idx).c_str());
      ROS_ERROR("available frames:");
      for (const std::string& s: link_names)
        ROS_ERROR("- %s",s.c_str());
      return false;
    }
  }

  m_q.resize(m_nax);
  m_Dq.resize(m_nax);
  m_pubs_in_base.resize(m_frame_idxs.size());
  m_pubs_in_link.resize(m_frame_idxs.size());
  for (unsigned int idx=0;idx<m_frames.size();idx++)
  {
    m_pubs_in_base.at(idx) =  controller_nh.advertise<geometry_msgs::TwistStamped>("/"+m_frames.at(idx)+"/twist_in_link",1);
    m_pubs_in_link.at(idx) =  controller_nh.advertise<geometry_msgs::TwistStamped>("/"+m_frames.at(idx)+"/twist_in_tool",1);
  }
  return true;
}

void RobotStateController::starting ( const ros::Time& time )
{

}

void RobotStateController::update ( const ros::Time& time, const ros::Duration& period )
{

  for (std::size_t idx=0;idx<m_nax;idx++)
  {
    m_q(idx) = m_jh.at(idx).getPosition();
    m_Dq(idx) = m_jh.at(idx).getVelocity();
  }

  std::vector<Eigen::Affine3d,Eigen::aligned_allocator<Eigen::Affine3d>> T_base_links=m_chain->getTransformations(m_q);

  std::vector< Eigen::Vector6d,Eigen::aligned_allocator<Eigen::Vector6d>> twists=m_chain->getTwist(m_q,m_Dq);

  for (unsigned int idx=0;idx<m_frames.size();idx++)
  {

    Eigen::Vector6d& twist_of_link_in_base=twists.at(m_frame_idxs.at(idx));

    Eigen::Vector6d twist_of_link_in_link=rosdyn::spatialRotation(twist_of_link_in_base,T_base_links.at(idx).linear().inverse());

    geometry_msgs::TwistStampedPtr msg=boost::make_shared<geometry_msgs::TwistStamped>();
    msg->twist.linear.x=twist_of_link_in_base(0);
    msg->twist.linear.y=twist_of_link_in_base(1);
    msg->twist.linear.z=twist_of_link_in_base(2);

    msg->twist.angular.x=twist_of_link_in_base(3);
    msg->twist.angular.y=twist_of_link_in_base(4);
    msg->twist.angular.z=twist_of_link_in_base(5);

    msg->header.stamp=ros::Time::now();
    msg->header.frame_id=m_base_frame;
    m_pubs_in_base.at(idx).publish(msg);

    geometry_msgs::TwistStampedPtr msg_in_link=boost::make_shared<geometry_msgs::TwistStamped>();
    msg_in_link->twist.linear.x=twist_of_link_in_link(0);
    msg_in_link->twist.linear.y=twist_of_link_in_link(1);
    msg_in_link->twist.linear.z=twist_of_link_in_link(2);

    msg_in_link->twist.angular.x=twist_of_link_in_link(3);
    msg_in_link->twist.angular.y=twist_of_link_in_link(4);
    msg_in_link->twist.angular.z=twist_of_link_in_link(5);

    msg_in_link->header.stamp=ros::Time::now();
    msg_in_link->header.frame_id=m_frames.at(idx);
    m_pubs_in_link.at(idx).publish(msg_in_link);

  }
}

void RobotStateController::stopping ( const ros::Time& time )
{


}




}
