#ifndef itia_fuzzy_controller_201925061154
#define itia_fuzzy_controller_201925061154

# include <std_msgs/Float64MultiArray.h>
# include <controller_interface/controller.h>
# include <itia_basic_hardware_interface/posveleff_command_interface.h>
# include <subscription_notifier/subscription_notifier.h>
# include <ros/ros.h>
# include <ros/package.h>
# include <ros/callback_queue.h>
# include <fl/Headers.h>

namespace itia
{
  namespace control
  {

    class FuzzyController : public controller_interface::Controller<hardware_interface::JointStateInterface>
    {
    public:
      bool init(hardware_interface::JointStateInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
      void update(const ros::Time& time, const ros::Duration& period);
      void starting(const ros::Time& time);
      void stopping(const ros::Time& time);


    protected:



      hardware_interface::JointStateInterface* m_hw;
      ros::NodeHandle m_root_nh;
      ros::NodeHandle m_controller_nh;

      ros::CallbackQueue m_queue;
      bool m_is_input_received;

      std::shared_ptr<ros_helper::SubscriptionNotifier<std_msgs::Float64MultiArray>> m_input_sub;
      ros::Publisher m_output_pub;
      std_msgs::Float64MultiArray m_output_msg;


      //Fuzzy variables
      fl::Engine* m_fz_engine;
      std::vector<fl::InputVariable*> m_fz_input;
      std::vector<fl::OutputVariable*> m_fz_output;
      std::string m_fz_file_name;
      std::vector<std::string> m_fuzzy_input_name;
      std::vector<std::string> m_fuzzy_output_name;

      int m_nIn;
      int m_nOut;

      int m_mode_set;

      void setInputCallback(const std_msgs::Float64MultiArrayConstPtr& msg);
      ~FuzzyController();

    };


  }
}


#endif
