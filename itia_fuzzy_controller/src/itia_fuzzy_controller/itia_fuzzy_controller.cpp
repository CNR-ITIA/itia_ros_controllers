#include <itia_fuzzy_controller/itia_fuzzy_controller.h>
#include <pluginlib/class_list_macros.h>


PLUGINLIB_EXPORT_CLASS(itia::control::FuzzyController, controller_interface::ControllerBase)


namespace itia
{
namespace control
{

FuzzyController::~FuzzyController()
{
}

bool FuzzyController::init(hardware_interface::JointStateInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{

  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_hw = hw;

  m_controller_nh.setCallbackQueue(&m_queue);


  if (!m_controller_nh.getParam("fuzzy_input_name",m_fuzzy_input_name))
  {
    ROS_ERROR("Parameter %s/fuzzy_input_name not defined. Set equal to the input of .fis file.", m_controller_nh.getNamespace().c_str());
    return false;
  }

  if (!m_controller_nh.getParam("fuzzy_output_name",m_fuzzy_output_name))
  {
    ROS_ERROR("Parameter %s/fuzzy_output_name not defined. Set equal to the output of .fis file.", m_controller_nh.getNamespace().c_str());
    return false;
  }

  if (!m_controller_nh.getParam("fuzzy_file_name", m_fz_file_name))
  {
    ROS_FATAL("Parameter %s/fuzzy_file_name does not exist.", m_controller_nh.getNamespace().c_str());
    return false;
  }

  if (!m_controller_nh.getParam("mode_set", m_mode_set))
  {
    ROS_FATAL("Parameter %s/mode_set does not exist, set default to 1.", m_controller_nh.getNamespace().c_str());
    m_mode_set = 1;
  }


  //Fuzzy Init
  m_fz_engine = new fl::Engine;
  try
  {
    m_fz_engine = fl::FisImporter().fromFile(ros::package::getPath("itia_fuzzy_controller")+"/config/fis/"+m_fz_file_name+".fis");
  }
  catch(...)
  {
    ROS_ERROR("Something wrong during .fis file import.");
    return false;
  }

  m_nIn = m_fuzzy_input_name.size();
  m_nOut = m_fuzzy_output_name.size();

  m_fz_input.resize(m_nIn);
  m_fz_output.resize(m_nOut);
  m_output_msg.data.resize(m_nOut);

  for (int i=0; i<m_nIn; i++)
  {
    m_fz_input.at(i) = new fl::InputVariable;
    m_fz_input.at(i) = m_fz_engine->getInputVariable(m_fuzzy_input_name.at(i));
  }

  for (int i=0; i<m_nOut; i++)
  {
    m_fz_output.at(i) = new fl::OutputVariable;
    m_fz_output.at(i) = m_fz_engine->getOutputVariable(m_fuzzy_output_name.at(i));

  }

  std::string status; //TODO VERIFICARE status
  if (!m_fz_engine->isReady(&status))
  {
    ROS_ERROR("[engine error] engine is not ready.");
    return false;
  }


  std::string fuzzy_input_topic;
  std::string fuzzy_output_topic;
  if (!m_controller_nh.getParam("fuzzy_input_topic", fuzzy_input_topic))
  {
    ROS_ERROR("%s/fuzzy_input_topic does not exist.", m_controller_nh.getNamespace().c_str());
    return false;
  }

  if (!m_controller_nh.getParam("fuzzy_output_topic", fuzzy_output_topic ))
  {
    ROS_ERROR("%s/fuzzy_output_topic does not exist.", m_controller_nh.getNamespace().c_str());
    return false;
  }

  m_input_sub.reset(new ros_helper::SubscriptionNotifier<std_msgs::Float64MultiArray>(m_controller_nh,fuzzy_input_topic,1));
  m_input_sub->setAdvancedCallback(boost::bind(&itia::control::FuzzyController::setInputCallback,this,_1));
  m_output_pub = m_root_nh.advertise<std_msgs::Float64MultiArray>(fuzzy_output_topic,1);

  m_is_input_received = false;

  ROS_INFO("Controller '%s' well initialized",m_controller_nh.getNamespace().c_str());
  return true;

}



void FuzzyController::starting(const ros::Time& time)
{
  ROS_INFO("Controller '%s' well started",m_controller_nh.getNamespace().c_str());
}



void FuzzyController::stopping(const ros::Time& time)
{
  m_queue.callAvailable();  //TODO DA RIVEDERE
  ROS_INFO("[ %s ] Stopping controller", m_controller_nh.getNamespace().c_str());
}



void FuzzyController::update(const ros::Time& time, const ros::Duration& period)
{
  m_queue.callAvailable();  //TODO DA RIVEDERE


  if (!m_is_input_received)
  {
    ROS_WARN_THROTTLE(1.0,"No input received yet. Something wrong in input callback");
    return;
  }

  //Fuzzy start run here
  m_fz_engine->process();

  for (int i=0; i<m_nOut; i++)
  {
    m_output_msg.data.at(i) = m_fz_output.at(i)->getValue();

    if (m_mode_set == 2)
        m_output_msg.data.at(i) = m_fz_input.at(0)->getValue();

    if (abs(m_output_msg.data.at(i)) > 0.8)  //Saturation
    {
//      m_output_msg.data.at(i) =  0.8 - 0.1 * (m_fz_input.at(0)->getValue());
        m_output_msg.data.at(i) =  0.8;// * (0.8 / m_output_msg.data.at(i));
    }

  }

  m_output_pub.publish(m_output_msg);

}



void FuzzyController::setInputCallback(const std_msgs::Float64MultiArrayConstPtr& msg)
{
  for (int i=0; i<m_nIn; i++)
    m_fz_input.at(i)->setValue(msg->data.at(i));

  m_is_input_received = true;

}



}
}
